document.addEventListener('DOMContentLoaded', pageLoaded, false);

var gStorageData = {};
var gDataSaved = false;

function mylog() {
    // var args = [].slice.call(arguments); //Convert to array
    // args.unshift('GS Booster:');
    // console.log.apply(this, args);
}

function pageLoaded()
{
    var initCycle = setInterval(function() {
        var url = window.location.href;

        if(url.match(/my-challenges\/current/) && $('.my-challenges__items').length) {

            contentLoaded();
            // clearInterval(initCycle);
        }

    }, 1000);

    var reloadCycle = setInterval(detectRefresh, 5000);
}

function contentLoaded()
{
    if($('#GSBooster-Loaded').length) {
        return;
    }
    $('.my-challenges__items').first().append('<div id="GSBooster-Loaded" class="GSBooster-x"></div>');

    loadStorageData(function() {
        hideBanner();
        showNotifications();

        showVotes();
        saveVotes();

        saveStorageData();
    });
}

function loadStorageData(callback)
{
    chrome.storage.local.get('GSBoosterData', function(result) {
        gStorageData = result.GSBoosterData;

        mylog('loadStorageData', gStorageData);

        if(!gStorageData || !gStorageData.installed) {
            gStorageData = {
                installed: true,
                votes: {},
                settings: {}
            };
        }

        callback();
    });
}

function saveStorageData()
{
    if(!gDataSaved) {
        chrome.storage.local.set({'GSBoosterData': gStorageData}, function() {
            gDataSaved = true;
        });
    }
}

function reloadContent()
{
    $('.GSBooster-x').remove();
    contentLoaded();
}

function hideBanner()
{
    var $banner = $('challenges-exhibition-banner').first();

    var $showHideBanner = $('<a>').text('Show Banner');
    var $showHideBannerDiv = $('<div>').addClass('GSBooster-ShowHideBanner GSBooster-x').append($showHideBanner);

    $showHideBanner.click(function() {
        if($(this).text() == 'Show Banner') {
            $(this).text('Hide Banner');
            $banner.show();
        }
        else {
            $(this).text('Show Banner');
            $banner.hide();
        }
    });

    $banner.before($showHideBannerDiv)
    $banner.hide();
}

function showNotifications()
{
    var boosts = $('.my-challenges__items').first().find('.gs-btn--type1').filter(function(i, btn) {
        return $(btn).find('.icon-boost').length;
    });


    var usedBoosts = 0;
    var lockedBoosts = 0;
    var missedBoosts = 0;
    var openBoosts = 0;

    for(var i = 0; i < boosts.length; i++) {
        var $b = $(boosts[i]);
        if($b.hasClass('boost-state-used')) usedBoosts++;
        else if($b.hasClass('boost-state-locked')) lockedBoosts++;
        else if($b.hasClass('boost-state-missed')) missedBoosts++;
        else openBoosts++;
    }

    $lastButton = $('.gs-btn-shop').last();

    $votesArea = $lastButton.clone().attr('id', 'GSBooster-TotalPlus').addClass('GSBooster-x');
    $votesArea.find('i').first().removeClass('icon-key').addClass('icon-vote-menu');
    $votesArea.find('.number').first().html('+<span id="GSBooster-TotalPlusValue"></span>');
    $votesArea.find('.name').first().html('votes since last visit');
    $votesArea.find('.plus').first().remove();
    $lastButton.after($votesArea);

    if(openBoosts) {
        $boostArea = $lastButton.clone().attr('id', 'GSBooster-NotifBoost').addClass('GSBooster-x');
        $boostArea.find('i').first().removeClass('icon-key').addClass('icon-boost');
        $boostArea.find('.number').first().text(openBoosts);
        $boostArea.find('.name').first().text('open ' + (openBoosts>1 ? 'boosts' : 'boost'));
        $boostArea.find('.plus').first().remove();
        $votesArea.after($boostArea);
    }
}

function showVotes()
{
    var totalPlus = 0;

    $('.my-challenges__item').each(function(i, challenge) {
        var $ch = $(challenge);

        var mobile = false;
        var speed = false;

        var name = _getChallengeName($ch);
        var total = _getChallengeVotes($ch);

        var $votes = $ch.find('.c-challenges-item__votes__total').first();
        if($votes.length < 1) {
            //speed challenge
            $votes = $ch.find('.round-progress-wrapper').first();
            speed = true;
        }
        if($votes.length < 1) {
            //mobile view, normal
            $votes = $ch.find('.votes').first();
            mobile = true;
        }

        _getVotesHistory(name, total, function(since) {
            // mylog(name, total, totalPlus, since);

            totalPlus += (since.last ? since.last : 0);

            $votes.append(
                '<div class="GSBooster-VotesSince GSBooster-x ' + (mobile?'GSBooster-VotesSince--mobile':'') + ' ' + (speed?'GSBooster-VotesSince--speed':'') + '">' +
                '  <div class="GSBooster-Since' + (since.last > 0 ? 'Plus' : 'Zero') + '">' +
                '    +' + since.last + ' since last visit' +
                '  </div>' +
                '</div>'
            );
        });
    });

    // totalPlus = parseInt(Math.random() * 100); //debug

    $('#GSBooster-TotalPlusValue').text(totalPlus);

    $('#GSBooster-TotalPlus').removeClass('GSBooster-TotalPlus--positive');
    if(totalPlus > 0) {
        $('#GSBooster-TotalPlus').addClass('GSBooster-TotalPlus--positive');
    }
}

function saveVotes()
{
    var votes = _getCurrentVotes();

    gStorageData.votes = {};
    for(var k in votes) {
        if(!k) continue;

        gStorageData.votes[k] = {
            last: votes[k]
        };
    }
}

function _getChallengeName($challenge)
{
    var name = $challenge.find('.c-challenges-item__title__label').first().text().trim();

    if(!name) {
        // speed challenge
        name = $challenge.find('.c-challenges-speed-item__title__header').first().text().trim();
    }

    if(!name) {
        //mobile view, normal
        name = $challenge.find('.c-challenges-item-mobile__title').first().text().trim();
    }

    return name ? name : null;
}

function _getChallengeVotes($challenge)
{
    var $votes = $challenge.find('.c-challenges-item__votes__total').first();
    var total = $votes.first('span').text().trim();

    if(!total) {
        //speed challenge
        total = $challenge.find('.c-challenges-item__photos__photo__votes').first().text().trim();
    }

    if(!total) {
        //mobile view, normal
        $votes = $challenge.find('.votes').first();
        total = $votes.text().trim().replace(/,/, '');
    }

    return total ? total : 0;
}

function _getCurrentVotes()
{
    var data = {};
    var votes = $('.my-challenges__item').each(function(i, challenge) {
        var $ch = $(challenge);

        var name = _getChallengeName($ch);
        var total = _getChallengeVotes($ch);

        if(name && total) {
            data[name] = parseInt(total);
        }
    });

    return data;
}

function _getVotesHistory(name, current, callback)
{
    var history = gStorageData.votes[name] || {};

    var last, yest;
    if(history) {
        last = history.last;
        yest = history.yest ? history.yest : last;
    }
    else {
        last = current;
        yest = current;
    }

    callback({
        last: parseInt(current) - parseInt(last),
        yesterday: parseInt(current) - parseInt(yest)
    });
}

function detectRefresh()
{
    if(!(gStorageData && gStorageData.votes)) {
        return false;
    }

    var votes = _getCurrentVotes();
    for(var k in votes) {
        if(gStorageData.votes[k] && votes[k] != gStorageData.votes[k].last) {
            $('.GSBooster-x').remove();
            return true;
        }
    }
    return false;
}